﻿(function($){

	$.fn.cookieTab = function(options){
		// set defaults
		var defaults = {
			activeTab: 0, //set active-tab
			tabHeaderElm: '.tabHeader',
			tabPanelElm: '.tabPanel',
			cookie: { name: 'jcookieTab', expires: 7 }
		};
		var options = $.extend(true, defaults, options);

		return this.each(function(){

			// prepare elements
			var tabValue;
			var $unit = $(this);
			var $cookieName = options.cookie.name;
			if($unit.attr('id')){$cookieName += '-' + $unit.attr('id');}
			var $tabHeader = $(options.tabHeaderElm + ' > li', $unit);
			var $tabPanel = $(options.tabPanelElm + ' > div', $unit);

			// execute
			init();

			// init
			function init(){
				if(options.cookie){tabValue = getCookie($cookieName);}
				if(!tabValue || tabValue==undefined){tabValue = options.activeTab;}
				tabControl();
			}

			// control tab-action
			function tabControl(){
				tabDisplay();
				$tabHeader.find('a').each(function(value){
					$(this).click(function(){
						if(options.cookie){setCookie($cookieName, value, options.cookie.expires, location.hostname, options.cookie.path, options.cookie.secure);}
						if(tabValue != value){
							tabValue = value;
							tabDisplay();
							return false;
						}
					});
				});
			}

			// control tab-view
			function tabDisplay(){
				$tabHeader.each(function(){
					$(this).removeClass('active');
				});
				$tabPanel.each(function(){
					$(this).hide();
				});
				$tabHeader.eq(tabValue).addClass('active');
				$tabPanel.eq(tabValue).show();
			}

			//set cookie
			function setCookie(ckName, ckValue, ckExpires, ckDomain, ckPath, ckSecure){
				var date = new Date();
				date.setTime(date.getTime() + ckExpires*24*60*60*1000);
				var ckStr = escape(ckName) + '=' + escape(ckValue); 
				ckStr += '; expires=' + date.toGMTString();
				if(ckDomain){ckStr += '; domain=' + ckDomain;}
				if(ckPath)  {ckStr += '; path=' + ckPath;}
				if(ckSecure){ckStr += '; secure';}
				document.cookie = ckStr;
			}

			//get cookie
			function getCookie(ckName){
				var ckMatch = ('; ' + document.cookie + ';').match('; ' + ckName + '=(.*?);');
				var ckValue = (ckMatch) ? ckMatch[1] : '';
				ckValue = unescape(ckValue);
				return ckValue;
			}

		});
	};
})(jQuery);

$(function(){
	$('#cookieTab').cookieTab();
});
